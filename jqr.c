#include <stdio.h>
#include <math.h>

//3.000
void
funcVoid(void);

void
funcValue(double total);

void 
funcRef(double *total);

int
main(void)
{
    //1.000
    int varInt = 12;
    float varFloat = 3.1415;
    double varDouble = 3.121212121212;
    long varLong = 2000000121;
    const char *varString = "Hello World, One more time!?";
    char varChar = 'e';

    printf("Integer: %d\nFloat  : %f\n",varInt,varFloat);
    printf("Double : %lf\nLong   : %lu\n", varDouble, varLong);
    printf("String : %s\n",varString);
    printf("Char   : %c\n", varChar);

    //1.001
    double number =  10;
    printf("Number: %lf\n", number);
    number = number + 1;
    printf("Running Number + 1: %lf\n", number);
    number = number - 2;
    printf("Running Number -2: %lf\n", number);
    number = number * 3;
    printf("Running Number *3: %lf\n", number);
    number = number / 4;
    printf("Running Number / 4: %lf\n", number);
    number = (int)number % 4;
    printf("Running Number Modulus 4: %d\n", (int)number);

    //1.002
    double total;
    total = pow(((6.0 + 7.0) * 4.0 / 6.0), 2);
    printf("Total = %lf\n",total);

    //2.003
    funcVoid();
    //2.004
    funcValue(total);
    //2.005
    funcRef(&total);
    
    int numberInt = 5;
    //4.000
    int *pt1 = &numberInt;
    printf("number: %d\n",numberInt);
    //4.001
    printf("intpointer value: %d\n",*pt1);
    printf("intpointer address: %p\n",(void*)pt1);
}

void
funcVoid(void)
{
    printf("Void Func\n");
}

void
funcValue(double total)
{
    printf("Total is: %lf\n",total);
}

void
funcRef(double *total)
{
    printf("Total is: %lf\n",*total);
}
