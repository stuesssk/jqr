#!/usr/bin/env python3
import sys

def printfunc():
    print("Hello World, AGAIN!!")

def printarguments(totalarguments):
    for arguments in totalarguments:
        print(arguments )

def power(number):
    number *=number
    return number

def multiply(number):
    number = number * 2
    power(number)
    return number
    

def main():
    #1.000
    varInteger = 13
    varFloat = 3.14
    varDouble = 3.1415926
    varString = "Hello World!"
    varChar = 'e'

    print(varInteger, type(varInteger))
    print(varFloat, type(varFloat))
    print(varDouble, type(varDouble))
    print(varString, type(varString))
    print(varChar, type(varChar))

    #1.001
    result = (varInteger * varFloat) / varFloat + 3 - 5
    print("Result = ", result)

    #2.000
    namesList = []
    with open(sys.argv[1]) as aFile:
            theLines = aFile.readlines()

    for eachLine in theLines:
            print(eachLine)

    #2.001
    f = open('tester.txt', 'w')
    f.write('Contents replaced.')
    f.close()

    #2.002
    new = open('Newfile.txt','w')
    new.close()

    #3.000
    for x in range (0,10):
        print(x) 

    #3.001
    i = 0
    while i <=10:
        print("i = ",i)
        i += 1

    #4.000
    if x == 9:
        print ("X = 9")


    if i > 10:
        print("i is graeter than 10")
    #4.001
    elif i < 10:
        print("i is less than 10")
    #4.002
    else:
        print("i equals 10")

    #5.000
    printfunc()

    #5.001
    arguments = sys.argv
    printarguments(arguments)

    #5.002
    number = 2
    print("Number = ", number)
    print("Number ^2 = ",power(number))

    #5.003
    number2 = 5
    print("number = ", number2 )
    print("(number * 2)^2 = ", multiply(number2))

    #6.000/6.001
    while True:
        userinput = input("Enter a number(q to quit):")
        if userinput == "q":
            break

        try:
            userinput = int(userinput)
        except ValueError as valErr:
            print(valErr, "Value enterd is not a number")
            continue
        

if __name__ == "__main__": main()
